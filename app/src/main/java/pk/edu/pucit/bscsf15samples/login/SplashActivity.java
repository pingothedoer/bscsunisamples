package pk.edu.pucit.bscsf15samples.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import pk.edu.pucit.bscsf15samples.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        SharedPreferences preferences =
                getSharedPreferences(Constants.PREF_NAME,
                        MODE_PRIVATE);


        boolean isLoggedIn = preferences.getBoolean(Constants.PREF_KEY, false);

        Intent intent;
        if (isLoggedIn){
            intent = new Intent(this, WelcomeActivity.class);
        }else{
            intent = new Intent(this, LoginActivity.class);
        }

        startActivity(intent);


    }

}
