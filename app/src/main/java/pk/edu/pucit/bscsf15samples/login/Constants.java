package pk.edu.pucit.bscsf15samples.login;

/**
 * Created by : Muhammad Ali Ansari,
 * Dated : 9/26/2018.
 */
public class Constants {

    public static final String PREF_KEY = "LOGGED_IN";
    public static final  String PREF_NAME = "pk.edu.pucit.bscsf15samples.login";
    public static final String FIRST_NUMBER = "First_number";
    public static final String SECOND_NUMBER = "Second_number";
    public static final int REQUEST_CODE_ADD = 100;
    public static final String RESULT = "RESULT";
}
