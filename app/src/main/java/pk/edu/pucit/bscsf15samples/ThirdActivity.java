package pk.edu.pucit.bscsf15samples;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class ThirdActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        SharedPreferences preferences = getSharedPreferences("bscsf15samples_preferences", Context.MODE_PRIVATE);

        String firstName = preferences.getString("FIRST_NAME", "");
        String lastName = preferences.getString("LAST_NAME", "");
        String email = preferences.getString("EMAIL", "");
        String password = preferences.getString("PASSWORD", "");
        String phoneNumber = preferences.getString("PHONE_NUMBER", "");

        Toast.makeText(getApplicationContext(), firstName + " : " + lastName + " : " + email + " : " + password + " : " + phoneNumber, Toast.LENGTH_LONG).show();


    }
}
