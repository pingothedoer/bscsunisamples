package pk.edu.pucit.bscsf15samples;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pk.edu.pucit.bscsf15samples.login.Constants;

/**
 * Created by : Muhammad Ali Ansari,
 * Dated : 9/12/2018.
 */
public class ResultActivity extends AppCompatActivity {

    private EditText thirdNumber;
    private Button btnShowResult;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_result);

        final String firstNumber = getIntent().getStringExtra(Constants.FIRST_NUMBER);
        final String secondNumber = getIntent().getStringExtra(Constants.SECOND_NUMBER);

        this.btnShowResult = (Button) findViewById(R.id.btnShowResult);
        this.thirdNumber = (EditText) findViewById(R.id.thirdNumber);

        btnShowResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String tNumber = thirdNumber.getText().toString();

                if (!TextUtils.isEmpty(tNumber)) {

                    int result = Integer.parseInt(firstNumber)
                            + Integer.parseInt(secondNumber)
                            + Integer.parseInt(tNumber);

                    Toast.makeText(ResultActivity.this, ""+result, Toast.LENGTH_SHORT).show();

                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(Constants.RESULT, result );
                    setResult(RESULT_OK, resultIntent);
                    finish();

                }

            }
        });


    }

}
