package pk.edu.pucit.bscsf15samples;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import pk.edu.pucit.bscsf15samples.login.Constants;

public class HomeActivity extends AppCompatActivity {

    private EditText firstNumber;
    private EditText secondNumber;
    private Button btnGetResult;
    private TextView resultTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.btnGetResult = (Button) findViewById(R.id.btnGetResult);
        this.secondNumber = (EditText) findViewById(R.id.secondNumber);
        this.firstNumber = (EditText) findViewById(R.id.firstNumber);
        this.resultTv = (TextView) findViewById(R.id.resultTv);

        btnGetResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String fNumber = firstNumber.getText().toString();
                String sNumber = secondNumber.getText().toString();

                if (!TextUtils.isEmpty(fNumber) && !TextUtils.isEmpty(sNumber)) {

                    Intent goIntent = new Intent(HomeActivity.this, ResultActivity.class);
                    goIntent.putExtra(Constants.FIRST_NUMBER, fNumber);
                    goIntent.putExtra(Constants.SECOND_NUMBER, sNumber);
                    startActivityForResult(goIntent, Constants.REQUEST_CODE_ADD);
                }

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE_ADD) {
            if (resultCode == RESULT_OK) {
                resultTv.setText(data.getStringExtra(Constants.RESULT));
            } else {
                resultTv.setText("No Result");
            }
        }

    }
}
