package pk.edu.pucit.bscsf15samples.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import pk.edu.pucit.bscsf15samples.R;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                SharedPreferences preferences = getSharedPreferences(Constants.PREF_NAME, MODE_PRIVATE);

                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Constants.PREF_KEY, true);
                editor.commit();

                startActivity(new Intent(LoginActivity.this, WelcomeActivity.class));


            }
        });
    }
}
