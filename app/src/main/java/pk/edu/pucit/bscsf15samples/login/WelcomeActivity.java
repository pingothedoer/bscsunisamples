package pk.edu.pucit.bscsf15samples.login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pk.edu.pucit.bscsf15samples.R;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
    }
}
