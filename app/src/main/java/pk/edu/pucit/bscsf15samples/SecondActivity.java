package pk.edu.pucit.bscsf15samples;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by : Muhammad Ali Ansari,
 * Dated : 9/12/2018.
 */
public class SecondActivity extends AppCompatActivity {

    private Button btnSubmit;
    private EditText etFirstName;
    private EditText etLastName;
    private ImageView imgAvatar;
    private EditText etEmail;
    private EditText etPassword;
    private TextView tvSelectGenderLabel;
    private RadioButton rdMale;
    private RadioButton rdFemale;
    private RadioGroup genderRadioGroup;
    private EditText etPhone;
    private CheckBox chkBoxTOS;
    private boolean isAgreed = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_layout);

        this.chkBoxTOS = (CheckBox) findViewById(R.id.chkBoxTOS);
        this.etPhone = (EditText) findViewById(R.id.etPhone);
        this.genderRadioGroup = (RadioGroup) findViewById(R.id.genderRadioGroup);
        this.rdFemale = (RadioButton) findViewById(R.id.rdFemale);
        this.rdMale = (RadioButton) findViewById(R.id.rdMale);
        this.tvSelectGenderLabel = (TextView) findViewById(R.id.tvSelectGenderLabel);
        this.etPassword = (EditText) findViewById(R.id.etPassword);
        this.etEmail = (EditText) findViewById(R.id.etEmail);
        this.imgAvatar = (ImageView) findViewById(R.id.imgAvatar);
        this.etLastName = (EditText) findViewById(R.id.etLastName);
        this.etFirstName = (EditText) findViewById(R.id.etFirstName);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (isValid()) {

                    String firstName = etFirstName.getText().toString();
                    String lastName = etLastName.getText().toString();
                    String email = etEmail.getText().toString();
                    String password = etPassword.getText().toString();
                    String phoneNumber = etPhone.getText().toString();

                    Intent myIntent = new Intent(SecondActivity.this, ThirdActivity.class);

                    SharedPreferences preferences = getSharedPreferences("bscsf15samples_preferences", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("FIRST_NAME", firstName );
                    editor.putString("LAST_NAME", lastName );
                    editor.putString("EMAIL", email );
                    editor.putString("PASSWORD", password );
                    editor.putString("PHONE_NUMBER", phoneNumber );
                    editor.commit();

                    startActivity(myIntent);

                }

            }

        });

        chkBoxTOS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    isAgreed = true;
                    btnSubmit.setEnabled(true);
                } else {
                    isAgreed = false;
                    btnSubmit.setEnabled(false);
                }
            }
        });


    }

    private boolean isValid() {

        if (TextUtils.isEmpty(etFirstName.getText().toString())) {
            etFirstName.setError("Enter First Name");
            return false;
        } else {
            etFirstName.setError(null);
        }

        if (TextUtils.isEmpty(etLastName.getText().toString())) {
            etLastName.setError("Enter last name");
            return false;
        } else {
            etLastName.setError(null);
        }

        if (TextUtils.isEmpty(etEmail.getText().toString())) {
            etEmail.setError("Enter your email address");
            return false;
        } else {
            etEmail.setError(null);
        }

        if (TextUtils.isEmpty(etPassword.getText().toString())) {
            etPassword.setError("Enter password");
            return false;
        } else {
            etPassword.setError(null);
        }

        if (TextUtils.isEmpty(etPhone.getText().toString())) {
            etPhone.setError("Please enter phone number");
            return false;
        } else {
            etPhone.setError(null);
        }

        if (!isAgreed) {
            return false;
        }

        return true;
    }
}
